package com.yaspo.games;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class DAOBorrower {

public Borrower getBorrower() {
		
		Borrower b = null; 
		
		try ( 	Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/games", "root", "");
				Statement s = con.createStatement();
				ResultSet r = s.executeQuery("SELECT borrower_name, city FROM borrower WHERE id=1");
			) 	
				{
					
					if (r.next()) {
						b = new Borrower();
						//b.setId(r.getInt("id"));
						b.setBorrower_name(r.getString("borrower_name"));
						//b.setStreet(r.getString("street"));
						//b.setHouse_number(r.getString("house_number"));
						//b.setBus_number(r.getString("bus_number"));
						//b.setPostcode(r.getInt("postcode"));
						b.setCity(r.getString("city"));
						//b.setTelephone(r.getString("telephone"));
						//b.setEmail(r.getString("email"));
					}
			
				}
		
		catch (Exception e) {
			e.printStackTrace();
		}
		
		return b;
		
	}	
	
}
