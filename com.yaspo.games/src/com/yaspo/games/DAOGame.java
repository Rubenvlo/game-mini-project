package com.yaspo.games;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class DAOGame {

	public Game getGame() {
		
		Game g = null; 
		
		try ( 	Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/games", "root", "");
				Statement s = con.createStatement();
				ResultSet r = s.executeQuery("SELECT * FROM game WHERE id=5");
			) 	
				{
					
					if (r.next()) {
						g = new Game();
						g.setId(r.getInt("id"));
						g.setGame_name(r.getString("game_name"));
						g.setAuthor(r.getString("author"));
						g.setYear_edition(r.getInt("year_edition"));
						g.setAge(r.getString("age"));
						g.setMin_players(r.getInt("min_players"));
						g.setMax_players(r.getInt("max_players"));
						g.setCategory_id(r.getInt("category_id"));
						g.setPlay_duration(r.getString("play_duration"));
						g.setDifficulty_id(r.getInt("difficulty_id"));
						g.setPrice(r.getDouble("price"));
						g.setImage(r.getString("image"));
						g.setDifficulty(InitializeDifficulty(g.getId()));
						g.setCategory(InitializeCategory(g.getId()));
					}
			
				}
		
		catch (Exception e) {
			e.printStackTrace();
		}
		
		return g;
			
	}

	public static Game searchGame(String columns, String name) {
		
		Game g = null;
		
		try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/games", "root", "");
			PreparedStatement ps = con.prepareStatement("SELECT " + columns + " FROM game WHERE game_name LIKE ?");
			)
		{
			
			ps.setString(1, "%" + name + "%");
			ResultSet r = ps.executeQuery();
			
			if (r.next()) {
				g = new Game();
				g.setId(r.getInt("id"));
				g.setGame_name(r.getString("game_name"));
				g.setEditor(r.getString("editor"));
				g.setAge(r.getString("age"));
				g.setPrice(r.getDouble("price"));
				g.setDifficulty(InitializeDifficulty(g.getId()));
				g.setCategory(InitializeCategory(g.getId()));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return g;
	}
	
	public static ArrayList<Game> searchGames(String columns, String name) {
		
		ArrayList<Game> ag = new ArrayList<Game>();
		
		try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/games", "root", "");
			PreparedStatement ps = con.prepareStatement("SELECT " + columns + " FROM game WHERE game_name LIKE ?");
			)
		{
			
			ps.setString(1, "%" + name + "%");
			ResultSet r = ps.executeQuery();
			
			while (r.next()) {
				Game g = new Game();
				g.setId(r.getInt("id"));
				g.setGame_name(r.getString("game_name"));
				g.setEditor(r.getString("editor"));
				g.setAge(r.getString("age"));
				g.setPrice(r.getDouble("price"));
				g.setDifficulty(InitializeDifficulty(g.getId()));
				g.setCategory(InitializeCategory(g.getId()));
				ag.add(g);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return ag;
	}
	
	public static ArrayList<Game> getAllGames() {
		
		ArrayList<Game> ag = new ArrayList<Game>();
		
		try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/games", "root", "");
			Statement s = con.createStatement();
			ResultSet r = s.executeQuery("SELECT game_name, editor, price FROM game ORDER BY game_name");
			)
		{
			while (r.next()) {
				Game g = new Game();
				g.setGame_name(r.getString("game_name"));
				g.setEditor(r.getString("editor"));
				g.setPrice(r.getDouble("price"));
				ag.add(g);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return ag;
		
	}
	
	private static Difficulty InitializeDifficulty(int game_id) {
		
		Difficulty d = null;
		
		try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/games", "root", "");
			Statement s = con.createStatement();
			ResultSet r = 
					s.executeQuery(	"SELECT difficulty.id, difficulty.difficulty_name FROM difficulty" + 
									" INNER JOIN game ON difficulty.id = game.difficulty_id" + 
									" WHERE game.id = " + game_id);
			)
		{
			
			if (r.next()) {
				d = new Difficulty();
				d.setId(r.getInt("id"));
				d.setDifficulty_name(r.getString("difficulty_name"));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return d;
		
	}
	
	private static Category InitializeCategory(int game_id) {
		
		Category c = null;
		
		try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/games", "root", "");
			Statement s = con.createStatement();
			ResultSet r = 
					s.executeQuery(	"SELECT category.id, category.category_name FROM category" + 
									" INNER JOIN game ON category.id = game.category_id" + 
									" WHERE game.id = " + game_id);
			)
		{
			
			if (r.next()) {
				c = new Category();
				c.setId(r.getInt("id"));
				c.setCategory_name(r.getString("category_name"));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return c;
		
	}
	
	private static Game createGameFromResultset(ResultSet r) {
		Game g = null;
		
		try {
			g = new Game();
			g.setId(r.getInt("id"));
			g.setGame_name(r.getString("game_name"));
			g.setEditor(r.getString("editor"));
			g.setAuthor(r.getString("author"));
			g.setYear_edition(r.getInt("year_edition"));
			g.setAge(r.getString("age"));
			g.setMin_players(r.getInt("min_players"));
			g.setMax_players(r.getInt("max_players"));
			g.setCategory_id(r.getInt("category_id"));
			g.setPlay_duration(r.getString("play_duration"));
			g.setDifficulty_id(r.getInt("difficulty_id"));
			g.setPrice(r.getDouble("price"));
			g.setImage(r.getString("image"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return g;
	}
	
	
	
}


