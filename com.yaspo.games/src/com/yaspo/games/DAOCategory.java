package com.yaspo.games;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class DAOCategory {

	public Category getCategory() {
		
		Category c = null; 
		
		try ( 	Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/games", "root", "");
				Statement s = con.createStatement();
				ResultSet r = s.executeQuery("SELECT * FROM category WHERE id=1");
			) 	
				{
					
					if (r.next()) {
						c = new Category();
						c.setId(r.getInt("id"));
						c.setCategory_name(r.getString("category_name"));
					}
			
				}
		
		catch (Exception e) {
			e.printStackTrace();
		}
		
		return c;
		
	}	
}
