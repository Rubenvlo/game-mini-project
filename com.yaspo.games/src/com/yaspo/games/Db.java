package com.yaspo.games;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Db {

	private String dbURL = "jdbc:mysql://localhost:3306/games";
	private String user = "root";
	private String password = "";
	private Connection con;
	
	public Db() {
		try {
			con = DriverManager.getConnection(dbURL, user, password);
		}
		catch (Exception e) {
			System.out.println("Error: " + e);
		}
		
	}
	
	public void printSearchResults (String search, ResultSet r, boolean columnNames) {
		try {
			if (!r.isBeforeFirst())
				System.out.println("No game with name " + search + " found!");
			else
				printAllColumns(r, columnNames);	
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void printSearchResults (String search, ResultSet r, String columns) {
		try {
			if (!r.isBeforeFirst())
				System.out.println("No game with name " + search + " found!");
			else
				printResultSetByColumns(r, columns);	
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public ResultSet getQueryResultSetWithParameters(String q, ArrayList<String> parameters) {
		
		ResultSet r = null;
		
		try {
			PreparedStatement ps = con.prepareStatement(q);
			
			for (int i = 0; i < parameters.size(); i++) {
				ps.setString(i + 1, parameters.get(i));
			}
			
			r = ps.executeQuery();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return r;
		
	}
	
	public ResultSet getQueryResultSet(String q) {
		
		ResultSet r = null;
		
		try {
			Statement s = con.createStatement();
			r = s.executeQuery(q);
		}
		catch (Exception e) {
			System.out.println("Error: " + e);
		}
		
		return r;
		
	}
	
	public void updateWithParameters(String q, ArrayList<String> parameters) {
		
		ResultSet r = null;
		
		try {
			PreparedStatement ps = con.prepareStatement(q);
			
			for (int i = 0; i < parameters.size(); i++) {
				ps.setString(i + 1, parameters.get(i));
			}
			
			ps.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	//specifically for adding a borrow
	public void updateWithParameters(String q, ArrayList<String> parameters, java.sql.Date d) {
		
		ResultSet r = null;
		
		try {
			PreparedStatement ps = con.prepareStatement(q);
			
			for (int i = 0; i < parameters.size(); i++) {
				ps.setString(i + 1, parameters.get(i));
			}
			
			ps.setDate(3, d);
			ps.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void printResultSetNoColumns(ResultSet r, String columns) {
		String[] c = columns.split(",");
		
		try {
			
			if (r != null) {
				while (r.next()) {
					
					for (int i = 0; i < c.length - 1; i++) {
						System.out.print(r.getString(c[i]) + ", ") ;
					}
					System.out.println(r.getString(c[c.length -1]));	
				}
				System.out.println();
			}
			else {
				System.out.println("No results found");
			}
			
		}
		catch (Exception e) {
			System.out.println("Error: " + e);
		}
	}
	
	public void printResultSetByColumns(ResultSet r, String columns) {
		
		String[] c = columns.split(",");
		
		try {
			
			if (r != null) {
				while (r.next()) {
					
					for (int i = 0; i < c.length - 1; i++) {
						System.out.print(c[i] + ": " + r.getString(c[i]) + ", ") ;
					}
					System.out.println(c[c.length - 1] + ": " + r.getString(c[c.length -1]));	
				}
				System.out.println();
			}
			else {
				System.out.println("No results found");
			}
			
		}
		catch (Exception e) {
			System.out.println("Error: " + e);
		}
		
	}
	
	public void printAllColumns(ResultSet r, boolean columnNames) {
		
		try {
			
			ResultSetMetaData meta = r.getMetaData();
			int columnCount = meta.getColumnCount();
			
			while (r.next()) {
				
				String toPrint = "";
				for (int i = 1; i <= columnCount; i++) {	
					if (columnNames) {
						toPrint += meta.getColumnName(i) + ": ";
					}
					toPrint += r.getString(i) + ", ";
				}
				toPrint = toPrint.substring(0, toPrint.length() - 2);
				System.out.println(toPrint);
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public static boolean hasColumn(ResultSet r, String column) throws SQLException {
		ResultSetMetaData meta = r.getMetaData();
	    int columnCount = meta.getColumnCount();
	    for (int x = 1; x <= columnCount; x++) {
	        if (column.equals(meta.getColumnName(x))) {
	            return true;
	        }
	    }
	    return false;
	}
	
}
