package com.yaspo.games;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;

public class Game {

	private int id;
	private String game_name;
	private String editor;
	private String author;
	private int year_edition;
	private String age;
	private int min_players;
	private int max_players;
	private int category_id;
	private String play_duration;
	private int difficulty_id;
	private double price;
	private String image;
	
	private Difficulty difficulty;
	private Category category;
	
	public Game() {
	}
	
	//assumes a full resultset from game table
	public Game(ResultSet r) {
		try {
			r.next();
			
			this.id = (Db.hasColumn(r, "id")) ? r.getInt("id") : -1;
			this.game_name = (Db.hasColumn(r, "game_name")) ? r.getString("game_name") : "";
			this.editor = (Db.hasColumn(r, "editor")) ? r.getString("editor") : "";
			this.author = (Db.hasColumn(r, "author")) ? r.getString("author") : "";
			this.year_edition = (Db.hasColumn(r, "year_edition")) ? r.getInt("year_edition") : -1;
			this.age = (Db.hasColumn(r, "age")) ? r.getString("age") : "";
			this.min_players = (Db.hasColumn(r, "min_players")) ? r.getInt("min_players") : -1;
			this.max_players = (Db.hasColumn(r, "max_players")) ? r.getInt("max_players") : -1;
			this.category_id = (Db.hasColumn(r, "category_id")) ? r.getInt("category_id") : -1;
			this.play_duration = (Db.hasColumn(r, "play_duration")) ? r.getString("play_duration") : "";
			this.difficulty_id = (Db.hasColumn(r, "difficulty_id")) ? r.getInt("difficulty_id") : -1;
			this.price = (Db.hasColumn(r, "price")) ? r.getDouble("difficulty_id") : -1;
			this.image = (Db.hasColumn(r, "image")) ? r.getString("image") : "";
			
			if (this.category_id > 0 && this.id > 0)
				InitializeCategory(id);
			if (this.difficulty_id > 0 && this.id > 0)
				InitializeDifficulty(id);
		}
		catch (Exception e){
			System.out.println("Error: " + e);
		}
	}
	
	public Game(int id, String game_name, String editor, String author, int year_edition, String age, int min_players, int max_players,
			int category_id, String play_duration, int difficulty_id, double price, String image) {
		this.id = id;
		this.editor = editor;
		this.game_name = game_name;
		this.author = author;
		this.year_edition = year_edition;
		this.age = age;
		this.min_players = min_players;
		this.max_players = max_players;
		this.category_id = category_id;
		this.play_duration = play_duration;
		this.difficulty_id = difficulty_id;
		this.price = price;
		this.image = image;
	}
	
	public String toString() {
		String s =  id + ", "
				+ game_name + ", "
				+ editor + ", "
				+ author + ", "
				+ year_edition + ", "
				+ age + ", "
				+ min_players + ", "
				+ max_players + ", "
				+ category_id + ", "
				+ play_duration + ", "
				+ difficulty_id + ", "
				+ price + ", "
				+ image;
		
		s = String.valueOf(s).replaceAll(", null", "");
		s = s.replaceAll(", 0", "");
		
		return s;
				
	}
	
	public String toStringDetailed() {
		String s =  id + ", "
				+ game_name + ", "
				+ editor + ", "
				+ author + ", "
				+ year_edition + ", "
				+ age + ", "
				+ min_players + ", "
				+ max_players + ", "
				+ category.getCategory_name() + ", "
				+ play_duration + ", "
				+ difficulty.getDifficulty_name() + ", "
				+ price + ", "
				+ image;
		
		s = String.valueOf(s).replaceAll(", null", "");
		s = s.replaceAll(", 0", "");
		
		return s;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getGame_name() {
		return game_name;
	}
	public void setGame_name(String game_name) {
		this.game_name = game_name;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public int getYear_edition() {
		return year_edition;
	}
	public void setYear_edition(int year_edition) {
		this.year_edition = year_edition;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public int getMin_players() {
		return min_players;
	}
	public void setMin_players(int min_players) {
		this.min_players = min_players;
	}
	public int getMax_players() {
		return max_players;
	}
	public void setMax_players(int max_players) {
		this.max_players = max_players;
	}
	public int getCategory_id() {
		return category_id;
	}
	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}
	public String getPlay_duration() {
		return play_duration;
	}
	public void setPlay_duration(String play_duration) {
		this.play_duration = play_duration;
	}
	public int getDifficulty_id() {
		return difficulty_id;
	}
	public void setDifficulty_id(int difficulty_id) {
		this.difficulty_id = difficulty_id;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getEditor() {
		return editor;
	}
	public void setEditor(String editor) {
		this.editor = editor;
	}

	public Difficulty getDifficulty() {
		return difficulty;
	}
	public void setDifficulty(Difficulty difficulty) {
		this.difficulty = difficulty;
	}
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	
	private  Difficulty InitializeDifficulty(int game_id) {
		
		Difficulty d = null;
		
		try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/games", "root", "");
			Statement s = con.createStatement();
			ResultSet r = 
					s.executeQuery(	"SELECT difficulty.id, difficulty.difficulty_name FROM difficulty" + 
									" INNER JOIN game ON difficulty.id = game.difficulty_id" + 
									" WHERE game.id = " + game_id);
			)
		{
			
			if (r.next()) {
				d = new Difficulty();
				d.setId(r.getInt("id"));
				d.setDifficulty_name(r.getString("difficulty_name"));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return d;
		
	}
	
	private Category InitializeCategory(int game_id) {
		
		Category c = null;
		
		try (Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/games", "root", "");
			Statement s = con.createStatement();
			ResultSet r = 
					s.executeQuery(	"SELECT category.id, category.category_name FROM category" + 
									" INNER JOIN game ON category.id = game.category_id" + 
									" WHERE game.id = " + game_id);
			)
		{
			
			if (r.next()) {
				c = new Category();
				c.setId(r.getInt("id"));
				c.setCategory_name(r.getString("category_name"));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return c;
		
	}
	
}
