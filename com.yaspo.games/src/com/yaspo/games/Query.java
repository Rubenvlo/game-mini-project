package com.yaspo.games;

public enum Query {

	DifficultyLevels ("SELECT difficulty_name FROM difficulty"),
	ShowBorrowers ("Select * FROM borrower"),
	SearchGameByID ("SELECT * FROM game WHERE id = ?"),
	CategoryWithID1 ("SELECT id, category_name FROM category WHERE id=1"),
	GameWithID5 ("SELECT * FROM game WHERE id=5"),
	BorrowerWithID1 ("SELECT borrower_name, city FROM borrower WHERE id=1"),
	OneGameWhereChoice ("SELECT game_name, editor, age, price FROM game WHERE game_name LIKE ? LIMIT 1"),
	ShowAllGames ("SELECT game_name, editor, price FROM game ORDER BY game_name"),
	SearchGames ("SELECT * FROM game WHERE game_name LIKE ? "),
	AddBorrow ("INSERT INTO borrow (game_id, borrower_id, borrow_date) VALUES (?, ?, ?)"),
	GamesWithCategoryAndDifficulty(
			"SELECT game.game_name, game.editor, game.author, game.editor, game.price , category.category_name, difficulty.difficulty_name FROM game " + 
			"INNER JOIN category ON game.category_id = category.id " + 
			"INNER JOIN difficulty on game.difficulty_id = difficulty.id " + 
			"WHERE game_name LIKE ?"),
	BorrowedGames (
			"SELECT game.game_name, borrower.borrower_name, borrow.borrow_date, borrow.return_date FROM borrow" + 
			" INNER JOIN game ON borrow.game_id = game.id" + 
			" INNER JOIN borrower ON borrow.borrower_id = borrower.id" + 
			" ORDER BY borrower.borrower_name, borrow.borrow_date"),
	CurrentBorrowers (
			"SELECT borrower.borrower_name FROM borrower" +
			" INNER JOIN borrow ON borrow.borrower_id = borrower.id" +
			" ORDER BY borrower.borrower_name"),
	FilterBorrowers (
			"SELECT game.game_name, borrower.borrower_name, borrow.borrow_date, borrow.return_date FROM borrow" + 
			" INNER JOIN game ON borrow.game_id = game.id" + 
			" INNER JOIN borrower ON borrow.borrower_id = borrower.id" +
			" WHERE borrower.borrower_name LIKE ?" +
			" ORDER BY borrower.borrower_name, borrow.borrow_date"),
	AtLeastDifficultyLevel (
			"SELECT game.game_name, difficulty.difficulty_name FROM game" +
			" INNER JOIN difficulty ON difficulty.id = game.difficulty_id" +
			" WHERE difficulty.id >= (SELECT id FROM difficulty WHERE difficulty_name = ?)"),
	SearchBorrower (
			"SELECT borrower_name, city, telephone, email FROM borrower" + 
			" WHERE borrower_name LIKE ?" + 
			" ORDER BY borrower_name"),
	BorrowerBefore (
			"SELECT borrower.borrower_name, borrow.borrow_date FROM borrower" +
			" INNER JOIN borrow ON borrow.borrower_id = borrower.id" +
			" WHERE borrow_date < ?"),
	SearchBorrowedGamesByID ( 
			"SELECT game.id, game.game_name, borrow.borrow_date FROM game" +
			" INNER JOIN borrow ON game.id = borrow.game_id" +
			" WHERE game.id = ?");
	
	private String queryString;
	private Query(String queryString) {
		this.queryString = queryString;
	} 
	
	public String getQuery() {
		return queryString;
	}
	
}
