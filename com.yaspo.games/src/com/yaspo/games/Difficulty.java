package com.yaspo.games;

public class Difficulty {

	private int id;
	private String difficulty_name;
	
	public Difficulty(int id, String difficulty_name) {
		this.id = id;
		this.difficulty_name = difficulty_name;
	}
	public Difficulty() {
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDifficulty_name() {
		return difficulty_name;
	}
	public void setDifficulty_name(String difficulty_name) {
		this.difficulty_name = difficulty_name;
	}
}
