package com.yaspo.games;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class DAODifficulty {

public Difficulty getDifficulty() {
		
		Difficulty d = null; 
		
		try ( 	Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/games", "root", "");
				Statement s = con.createStatement();
				ResultSet r = s.executeQuery("SELECT * FROM category WHERE id=1");
			) 	
				{
					
					if (r.next()) {
						d = new Difficulty();
						d.setId(r.getInt("id"));
						d.setDifficulty_name(r.getString("category_name"));
					}
			
				}
		
		catch (Exception e) {
			e.printStackTrace();
		}
		
		return d;
		
	}	
	
}
