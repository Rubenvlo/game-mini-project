package com.yaspo.games;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class Runner {

	static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		System.out.println("---Mini Games Project---");
		System.out.println("    --- By Ruben ---    ");
		
		Db db = new Db();
		
		boolean finished = false;
			
		while (!finished) {
			System.out.println("Choose an option by entering the corresponding number");
			displayMenu();
			int input = getUserChoice(12);
			executeOption(input, db);
			finished = promptProgramEnd();
		}
		
		sc.close();
		
	}
	
	static void displayMenu() {
		System.out.println("1. Show the first game category");
		System.out.println("2. Show the fifth game");
		System.out.println("3. Show the first borrower");
		System.out.println("4. Show a game of your choice");
		System.out.println("5. Show all games");
		System.out.println("6. Show a list of games and choose a game");
		System.out.println("7. Show borrowed games");
		System.out.println("8. Advanced search : difficulty");
		System.out.println("9. Complex search: borrowers");
		System.out.println("10. Complex search : borrowers before date");
		System.out.println("11. Administration loans");
	}
	
	static boolean promptProgramEnd() {
		System.out.println("Do you want to pick a new option? Y/N");
		
		String input = getUserInput();
		
		while (input.indexOf("Y") < 0 && input.indexOf("N") < 0) {
			System.out.println("Please enter Y for Yes or N for No");
			input = getUserInput();
		}
		
		if (input.indexOf("Y") >= 0)
			return false;
		else if (input.indexOf("N") >= 0)
			return true;
		else
			return false;
		
	}
	
	static void executeOption(int input, Db db) {
		
		String q = "";
		ArrayList<String> parameters = new ArrayList<String>();
		
		switch (input) {
			case 1:
				System.out.println("Fist game category: ");
				db.printAllColumns(db.getQueryResultSet(Query.CategoryWithID1.getQuery()), true);
				break;
			case 2:
				System.out.println("Fifth game: ");
				Game g2 = new Game(db.getQueryResultSet(Query.GameWithID5.getQuery()));
				System.out.println(g2.toString());
				break;
			case 3: 
				System.out.println("First borrower: ");
				db.printResultSetByColumns(db.getQueryResultSet(Query.BorrowerWithID1.getQuery()), "borrower_name,city");
				break;
			case 4: 
				System.out.println("Enter the game of your choice");
				String game4 = getUserInput();
				
				parameters.clear();
				parameters.add("%" + game4 + "%");
				db.printSearchResults(game4, db.getQueryResultSetWithParameters(Query.OneGameWhereChoice.getQuery(), parameters), "game_name,editor,age,price");
				
				break;
			case 5: 
				db.printResultSetNoColumns(db.getQueryResultSet(Query.ShowAllGames.getQuery()), "game_name,editor,price");
				break;
			case 6: 
				parameters.clear();
				parameters.add("%" + "lo" + "%");
				db.printSearchResults("lo", db.getQueryResultSetWithParameters(Query.SearchGames.getQuery(), parameters), "game_name,editor,age,price");
				
				System.out.println("Pick a game from the games above");
				
				String game6 = getUserInput();
				parameters.clear();
				parameters.add("%" + game6 + "%");
				db.printSearchResults(game6, db.getQueryResultSetWithParameters(Query.GamesWithCategoryAndDifficulty.getQuery(), parameters), false);
				
				break;
			case 7:
				
				db.printAllColumns(db.getQueryResultSet(Query.BorrowedGames.getQuery()), false);
				
				if (actionOrExit("filter by borrower")) {
					System.out.println("List of borrowers :");
					
					db.printAllColumns(db.getQueryResultSet(Query.CurrentBorrowers.getQuery()), false);
					
					System.out.println();
					System.out.println("Select a borrower from the list above to filter on");
					String borrower7 = getUserInput();
					
					parameters.clear();
					parameters.add("%" + borrower7 + "%");
					System.out.println("Filtered list");
					db.printAllColumns(db.getQueryResultSetWithParameters(Query.FilterBorrowers.getQuery(), parameters), false);
				}
				else {
					System.out.println("Exiting");
				}
				break;
			case 8: 
				System.out.println("Difficulty levels");
				db.printAllColumns(db.getQueryResultSet(Query.DifficultyLevels.getQuery()), false);
				
				System.out.println("Select difficulty level to filter on");
				String difficulty8 = getUserInput();
				
				parameters.clear();
				parameters.add(difficulty8);
				db.printAllColumns(db.getQueryResultSetWithParameters(Query.AtLeastDifficultyLevel.getQuery(), parameters), false);
				break;
			case 9:
				System.out.println("Enter part of a borrower name to search for");
				
				String borrower9 = getUserInput();
				
				parameters.clear();
				parameters.add("%"+ borrower9 +"%");
				db.printSearchResults(borrower9, db.getQueryResultSetWithParameters(Query.SearchBorrower.getQuery(), parameters), false);
				
				break;
			case 10:
				System.out.println("Enter a date in yyyy-MM-dd format");
				
				String date10 = getUserInput();
				
				parameters.clear();
				parameters.add(date10);
				db.printSearchResults(date10, db.getQueryResultSetWithParameters(Query.BorrowerBefore.getQuery(), parameters), false);
				
				break;
			case 11:
				
				db.printResultSetNoColumns(db.getQueryResultSet(Query.ShowAllGames.getQuery()), "id,game_name");
				
				System.out.println("Pick a game from the games above by id");
				
				String gameId11 = getUserInput();
				parameters.clear();
				parameters.add(gameId11);
				ResultSet r = db.getQueryResultSetWithParameters(Query.SearchBorrowedGamesByID.getQuery(), parameters);
				
				try {
					if (!r.isBeforeFirst()) {
						db.printResultSetNoColumns(db.getQueryResultSetWithParameters(Query.SearchGameByID.getQuery(), parameters), "id,game_name,editor,author,price");
						
						db.printResultSetNoColumns(db.getQueryResultSet(Query.ShowBorrowers.getQuery()), "id,borrower_name");
						System.out.println("Select a borrower from the list above by id");
						
						String borrowerId11 = getUserInput();
						parameters.add(borrowerId11);
						
						DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
						Date date11 = new Date();
						java.sql.Date sqlDate11 = new java.sql.Date(date11.getTime());
						
						db.updateWithParameters(Query.AddBorrow.getQuery(), parameters, sqlDate11);
						
						System.out.println("Updated borrow table");
						db.printAllColumns(db.getQueryResultSet(Query.ShowBorrowers.getQuery()), false);
						
					}
					else
						System.out.println("Game is already borrowed!");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				break;
			default:
				System.out.println("default");
				break;
				
				
//				DAO solution for 5. Show all games
//				ArrayList<Game> allGames = DAOGame.getAllGames();
//				for (Game g: allGames) {
//					System.out.println(g.toString());
//				}
				

//				DAO solution for 6. show a list of games and choose a game
//				DAOGame dao = new DAOGame();
//				ArrayList<Game> gs = dao.searchGames("id, game_name, editor, age, price" ,"lo");
//				for (Game g: gs) {
//					System.out.println(g.toString());
//				}
//				
//				System.out.println("Pick a game from the games above");
//				String game7 = getUserInput();
//				Game g7 = dao.searchGame("id, game_name, editor, age, price", game7);
//				System.out.println(g7.toStringDetailed()); 
				
		}
		
	}
	
	static int getUserChoice(int n) {
		
		String input = getUserInput(); 
	
		while (!validateUserChoice(input, n)) {
			input = getUserInput(); 
		}
		
		return Integer.parseInt(input);
	}
	
	static boolean validateUserChoice(String input, int n) {
		
		int x = 0;
		
		try {
			x = Integer.parseInt(input);
			if (x <= 0 || x > n) 
				throw new Exception();
		}
		catch (Exception e) {
			System.out.println("Invalid input, pick an option between 1 and " + n + "!");
			return false;
		}
		
		return true;
	}
	
	static String getUserInput() {

		String input = sc.nextLine();
		return input;
		
	}
	
	static boolean actionOrExit(String action) {
		System.out.println("Do you want to " + action + " or exit?");
		System.out.println("Enter \"exit\" to exit, anything else to continue");
		
		String input = getUserInput();
		if (input.indexOf("exit") >= 0) 
			return false;
		else
			return true;
		
	}	
}
