use games;
SELECT * FROM borrow ORDER BY borrow_date;

SELECT game.game_name, game.editor, game.author, category.category_name, difficulty.difficulty_name FROM game
INNER JOIN category ON game.category_id = category.id
INNER JOIN difficulty on game.difficulty_id = difficulty.id

SELECT difficulty.id, difficulty.difficulty_name FROM difficulty
INNER JOIN game ON difficulty.id = game.difficulty_id
WHERE game.id = 15

SELECT difficulty.id, difficulty.difficulty_name FROM difficulty 
				INNER JOIN game ON difficulty.id = game.difficulty_id 
								 WHERE game.id = 5
								 
								 
SELECT game.game_name, borrower.borrower_name, borrow.borrow_date, borrow.return_date FROM borrow
INNER JOIN game ON borrow.game_id = game.id
INNER JOIN borrower ON borrow.borrower_id = borrower.id

SELECT borrower.borrower_name FROM borrower
INNER JOIN borrow ON borrow.borrower_id = borrower.id
ORDER BY borrower.borrower_name

SELECT game.game_name, difficulty.difficulty_name FROM game
JOIN difficulty ON difficulty.id = game.difficulty_id
WHERE difficulty.id >= (SELECT id FROM difficulty WHERE difficulty_name = ?)

SELECT borrower_name, city, telephone, email FROM borrower
WHERE borrower_name LIKE ?

SELECT borrower.borrower_name, borrow.borrow_date FROM borrower
JOIN borrow ON borrow.borrower_id = borrower.id
WHERE borrow_date < ?

SELECT game.id, game.game_name, borrow.borrow_date FROM game
JOIN borrow ON game.id = borrow.game_id
WHERE game.id = ?


